#!/usr/bin/python
import sys
from os import listdir
from os.path import isfile, join
import subprocess


assemblies = []
csfiles = []
for arg in sys.argv[1:] :
	if (".cs" in arg):
		csfiles.append(arg)
	if ("-R:" in arg) :
		split = ""
		split += arg.split(":")[1]
		for filename in listdir(split):
			if (".dll" in filename and filename.find(".dll") + len(".dll") == len(filename)) :
				assemblies.append(split + "/" + filename)
				

compilationArgs = ["mcs"]
for file in csfiles:
	compilationArgs.append(file) 
#compilationArgs.extend(csfiles)
print ("files:")
print (compilationArgs)
for assembly in assemblies:
	compilationArgs.append("-r:" + assembly) 
compilationArgs.append("-r:" + "System.Drawing.dll")
compilationArgs.append("-r:" + "System.Windows.dll")
compilationArgs.append("-r:" + "System.Windows.Forms.dll")
compilationArgs.append("-r:" + "mscorlib.dll")
compilationArgs.append("-r:" + "System.Data.dll")
compilationArgs.append("-r:" + "System.Core.dll")
compilationArgs.append("-r:" + "System.Xml")
compilationArgs.append("-r:" + "System.Dynamic.dll")
compilationArgs.append("-r:" + "System")
#compilationArgs.append("-r:" + "System.Runtime")
#compilationArgs.append("-r:" + "System")
#System
#System.Data
#System.Drawing
#System.Windows.Forms
#System.Xml
#compilationArgs.append("-r:" + "System.Threading.dll")
print (",\n+ assemblies: ")
print(compilationArgs)
p2 = subprocess.Popen(["brew", "ls", "--versions", "mono"], stdout = subprocess.PIPE)
out1, err1 = p2.communicate()
if ("mono 4.0.1" in out1) :
	print ("ok")
else :
	p3 = subprocess.Popen(["brew", "install", "mono"], stdout=subprocess.PIPE)
	print(out1)
p = subprocess.Popen(compilationArgs, stdout=subprocess.PIPE)
out, err = p.communicate()
print(out)
print(err)

subprocess.call(compilationArgs)