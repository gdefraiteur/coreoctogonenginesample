using System;
using Assimp;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OctogonEngine;
using Vector3 = OctogonEngine.Vector3;
using Quaternion = OctogonEngine.Quaternion;
using Matrix = OctogonEngine.Matrix;

using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OctogonEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using OpenTK.Platform;
using Config = OpenTK.Configuration;
using Utilities = OpenTK.Platform.Utilities;
using Assimp;
using System.Diagnostics;
using System.Drawing;
using Mesh = OctogonEngine.Mesh;
using Material = Assimp.Material;
using System.IO;
using Vector2 = OctogonEngine.Vector2;
using System.Reflection;

public class Program
{
	public struct Byte4
	{
		public byte R, G, B, A;

		public Byte4(byte[] input)
		{
			R = input[0];
			G = input[1];
			B = input[2];
			A = input[3];
		}

		public uint ToUInt32()
		{
			byte[] temp = new byte[] { this.R, this.G, this.B, this.A };
			return BitConverter.ToUInt32(temp, 0);
		}

		public override string ToString()
		{
			return this.R + ", " + this.G + ", " + this.B + ", " + this.A;
		}
	}
	
	public static void Main(string[] argv)
	{
		Console.WriteLine("yo");
	}
}