using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OctogonEngine;
using System.Windows.Forms;

namespace OctogonEngine
{
	public enum Space
	{
		self, world
	}
	public class Transform : MonoBehaviour
	{
		private Quaternion _localRotation = Quaternion.identity;
		private Vector3 _localScale = Vector3.one;
		private Vector3 _localPosition = Vector3.zero;
		private Vector3 _position = Vector3.zero;
		private Quaternion _rotation = Quaternion.identity;
		private Matrix _transformation;
		private static List<Transform> _Selection = new List<Transform>();
		public static List<Transform> Selection
		{
			get
			{
				if (Transform._Selection == null)
					Transform._Selection = new List<Transform>();
				return (Transform._Selection);
			}
			set
			{
				List<Transform> selection = (List<Transform>)Transform._Selection.Clone();
				for (int i = 0; i < selection.Count; i++)
				{
					selection[i].isSelected = false;
				}
				Transform._Selection = value;
				for (int i = 0; i < Transform._Selection.Count; i++)
				{
					Transform._Selection[i].isSelected = true;
				}
			}
		}
		private static Transform _activeTransform = null;
		public static Transform activeTransform
		{
			get
			{
				return (Transform._activeTransform);
			}
			set
			{
				if (Transform._activeTransform != null && !Transform._Selection.Contains(Transform._activeTransform))
				{
					Transform._activeTransform.isSelected = false; 
				}
				Transform._activeTransform = value;
				Transform._activeTransform.isSelected = true;
			}
		}
		public static List<Transform> Transforms = new List<Transform>();
		private bool _isSelected = false;
		public bool isSelected
		{
			get
			{
				return (this._isSelected);
			}
			set
			{
				this._isSelected = value;
				if (this._isSelected && !Transform._Selection.Contains(this))
				{
					Transform._Selection.Add(this);
				}
				else if (!this._isSelected && Transform._Selection.Contains(this))
				{
					Transform._Selection.Remove(this);
				}
			}
		}

		public Matrix transformation
		{
			get
			{
				return (this._transformation);
			}
			set
			{
				Vector3 localPos = Vector3.zero;
				Vector3 localScale = Vector3.one;
				Quaternion localRot = Quaternion.identity;
				this._transformation = value;
				this._transformation.Decompose(out localScale, out localRot, out localPos);
				this._localPosition = localPos;
				this._localRotation = localRot;
				this._localScale = localScale;
				this._updateFromLocal();

			}
		}

		public Vector3 transformDirection(Vector3 direction)
		{
			return (this.transform.rotation * direction);
		}
		public Vector3 inverseTransformDirection(Vector3 direction)
		{
			return (this.transform.rotation.inverse * direction);
		}
		public Vector3 limitedTransformDirection(Vector3 localDirection)
		{
			return (this.transform._localRotation * localDirection);
		}
		public Vector3 lossyScale
		{
			get
			{
				Matrix trs = this.localToWorldMatrix;
				Vector3 scale;
				Quaternion rot;
				Vector3 pos;
				trs.Decompose(out scale, out rot, out pos);
				return (scale);
			}
			set
			{
				Vector3 scale = value;
				Vector3 pos = this.position;
				Matrix tmp = (Matrix.translation(this._localPosition).inverse * this._rotation.inverse.orthogonalMatrix) * this.parent.worldToLocalMatrix;
				Vector3 xvec = (new Vector3(scale.x, 0, 0)  + pos) * tmp;
				Vector3 yvec = (new Vector3(0, scale.y, 0) + pos) * tmp; //this.calMatrix;
				Vector3 zvec = (new Vector3(0, 0, scale.z)  + pos) * tmp;
				
				//if (scale.x < 0)

				Vector3 all = xvec + yvec + zvec;
				float x = all.x;
				float y = all.y;
				float z = all.z;
				if (x < 0)
				{
					x = -x;
				}
				if (y < 0)
				{
					y = -y;
				}
				if (z < 0)
				{
					z = -z;
				}
				all = new Vector3(x, y, z);

				this.localScale = all;
			}
		}
		public Vector3 limitedInverseTransformDirection(Vector3 parentDirection)
		{
			return (this.transform._localRotation.inverse * parentDirection);
		}
		private void _updateFromLocal()
		{
			//Debug.Log("hey");
			Matrix translation = Matrix.translation(this._localPosition);
			Matrix rotation = new Matrix(this._localRotation);
			Matrix scale = Matrix.scale(this._localScale);
			this._transformation = translation * rotation * scale;
			if (parent != null)
			{
				this._position = parent.localToWorldMatrix * this._localPosition;
				this._rotation = parent.transformRotation(this._localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
		}
		private void _updateFromWorld()
		{
			if (this.parent != null)
			{
				Matrix parenttransformation = this.parent.worldToLocalMatrix;
				_localPosition = parenttransformation * this._position;
				_localRotation = parent.inverseTransformRotation(this._rotation);
				Matrix translation = Matrix.translation(this._localPosition);
				Matrix rotation = new Matrix(this._localRotation);
				Matrix scale = Matrix.scale(this._localScale);
				this._transformation = translation * rotation * scale;
			
				//this._transformation.Decompose(out this._localScale, out this._localRotation, out this._localPosition);
			}
			else
			{
				this._localPosition = this._position;
				this._localRotation = this._rotation;
				Matrix translation = Matrix.translation(this._localPosition);
				Matrix rotation = new Matrix(this._localRotation);
				Matrix scale = Matrix.scale(this.localScale);
				this._transformation = translation * rotation * scale;
				//this._transformation.Decompose(out this._localScale, out this._localRotation, out this._localPosition);
			}
		}

		public Vector3 getPosition()
		{
			if (this.parent != null)
			{
				return (this.parent.transformPoint(this._localPosition));
			}
			else
			{
				return (this._localPosition);
			}
		}
		public Vector3 position
		{
			get
			{
				return (this.getPosition());
				/*this._updateFromLocal();
				return (this._position);*/
			}
			set
			{
				//Debug.Log("yoyo" + value);
				this._position = value;
				if (this.parent != null)
					this._localPosition = parent.inverseTransformPoint(this._position);
				else
					this._localPosition = value;
				this._updateFromLocal();
			}
		}
		public Vector3 localScale
		{
			get
			{
				return (this._localScale);
			}
			set
			{
				this._localScale = value;
				this._updateFromLocal();
			}
		}
		public Vector3 localPosition
		{
			get
			{
				return (this._localPosition);
			}
			set
			{
				this._localPosition = value;
				this._updateFromLocal();
			}
		}

		private OctogonEngine.Quaternion getRotation()
		{
			//Console.WriteLine("???");
			Transform tr = this;
			OctogonEngine.Quaternion quat = tr.localRotation;
			Transform current = tr;
			while (current.parent != null)
			{
				//Console.WriteLine("bah?");
				quat =  current.parent.localRotation * quat;
				current = current.parent;
			}
			//Console.WriteLine("oh");
			return (quat);
		}
		public Quaternion rotation
		{
			get
			{
				return (this.getRotation());
			}
			set
			{
				if (this.parent != null)
				{
					this._localRotation = this.parent.inverseTransformRotation(value);
				}
				else
				{
					this._localRotation = value;
				}
				this._updateFromLocal();
			}
		}
		public Quaternion localRotation
		{
			get
			{
				return (this._localRotation);
			}
			set
			{
				this._localRotation = value;
				this._updateFromLocal();
			}
		}
		public bool isChildOf(Transform candidate)
		{
			for (int i = 0; i < candidate.childCount; i++)
			{
				if ((MonoBehaviour)candidate.children[i] == (MonoBehaviour)this)
				{
					return (true);
				}
			}
			return (false);
		}
		public Matrix localToWorldMatrix
		{
			get
			{
				Matrix res = new Matrix(4, 4);
				if (this.parent != null)
					res =  this.parent.localToWorldMatrix * (this._transformation);
				else
					res = (this._transformation);
				return (res);
			}
			set
			{
				;
			}
		}
		public List<Transform> parentsRecursive()
		{
			Transform tmp = this.parent;
			List<Transform> res = new List<Transform>();
			while (tmp != null)
			{
				res.Add(tmp);
				tmp = tmp.parent;
			}
			return (res);
		}

		private Matrix getInverseTransformation()
		{
			if (this.transformation.inverse != Matrix.Zero)
			{
				return (this.transformation.inverse);
			}
			else
			{
				//this.Decompose(out rescale, out rot, out pos);
				Matrix translation = Matrix.translation(Vector3.zero - this._localPosition);
				Matrix rotation = this.localRotation.inverse.orthogonalMatrix;
				Matrix scale = Matrix.scale(new Vector3(1 / this._localScale.x, 1 / this._localScale.y, 1 / this._localScale.z));
				return (translation * rotation * scale);	
			}
		}
		public Matrix worldToLocalMatrix
		{
			get
			{
				if (parent != null)
				{
					Matrix res;
					List<Transform> parents = this.parentsRecursive();
					res = parents[parents.Count - 1].getInverseTransformation();
					for (int i = (parents.Count - 2); i >= 0; i--)
					{
						res = parents[i].getInverseTransformation() * res;
					}
					res = this.getInverseTransformation() * res;
					return (res);
				}
				else
				{
					Matrix res = this.getInverseTransformation();
					return (res);
				}
			}
			set
			{
				;
			}
		}
		public Transform root
		{
			get
			{
				if (this.parent == null)
					return (this);
				Transform tmp = this.parent;
				while (tmp.parent != null)
				{
					tmp = tmp.parent;
				}
				return (tmp);
			}
		}
		private Transform _parent;
		private int _siblingIndex;
		public int siblingIndex
		{
			get
			{
				return (this._siblingIndex);
			}
			set
			{
				;
			}
		}
		public string name;
		public float GetDist(Transform other)
		{
			return (other.position.GetDist(this.position));
		}
		public Transform parent
		{
			get
			{
				return (this._parent);
			}
			set
			{
				Vector3 position = this.position;
				Quaternion rotation = this.rotation;
				Vector3 scale = this._localScale;

				if (this._parent != null && this._parent.children != null)
				{
					this._parent.children.RemoveOnly(this);
				}
				this._parent = value;
				if (this._parent != null)
				{
					if (parent.children == null)
					{
						this._parent.children = new TransformCollection(this._parent);
					}
					this._siblingIndex = this._parent.children.Count;

					this._parent.children.Add(this);
					this.position = position;
					this.rotation = rotation;
					this.localScale = scale;
					//this._updateFromWorld();
				}

			}
		}
		public Assimp.Scene m_model;
		private TransformCollection _children;
		public TransformCollection children
		{
			get
			{
				if (this._children == null)
				{
					this._children = new TransformCollection(this);
				}
				return (this._children);
			}
			set
			{
				if (this._children != null)
				{
					for (int i = 0; i < this._children.Count; i++)
					{
						this._children[i].parent = null;
					}
				}
				this._children = value;
				for (int i = 0; i < this._children.Count; i++)
				{
					this._children[i].parent = this;
				}
			}
		}
		public Vector3 right
		{
			    get
			    {
					return (Vector3) (this.rotation * Vector3.right);
			    }
			    /*set
			    {
					this.rotation = Quaternion.LookRotation(value);
			    }*/
		}
		public Vector3 up
		{
			    get
			    {
					return (Vector3) (this.rotation * Vector3.up);
			    }
			    /*set
			    {
					this.rotation = Quaternion.LookRotation(value);
			    }*/
		}
		public Vector3 forward
		{
			    get
			    {
					return (Vector3) (this.rotation * Vector3.forward);
			    }
			    /*set
			    {
					this.rotation = Quaternion.LookRotation(value);
			    }*/
		}
		public void RotateAround(Vector3 worldPosition, Vector3 axis, float angl)
		{
				angl = (angl / 360) * (float)Math.PI * 2;
				Quaternion rotation1 = Quaternion.CreateFromAxisAngle(axis, angl);
				transform.rotation = (rotation1 * transform.rotation);
				transform.position = (rotation1 * (transform.position - worldPosition)) + worldPosition;
				this._updateFromWorld();
		}
		public Vector3 localEulerAngles
		{
			get
			{
				return (this.rotation.eulerAngles);
			}
			set
			{
				this._localRotation.eulerAngles = value;
				this._updateFromLocal();
			}
		}
		public int childCount
		{
			get
			{
				return (this.children.Count);
			}
			set
			{
				;
			}
		}
		public int ChildCount
		{
			get
			{
				return (this.children.Count);
			}
		}
		public Transform(Vector3 pos, Quaternion rotation, Vector3 scale)
		{
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this.children = new TransformCollection(this);
			this._parent = null;
			this._position = pos;
			this._rotation = rotation;
			this._localScale = scale;
			this.position = new Vector3(pos);
			this.rotation = new Quaternion(rotation);
			this.localScale = new Vector3(scale);
			this._updateFromLocal();
		}
		public Transform(Vector3 pos, Quaternion rotation, Vector3 scale, Space type)
		{
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this.children = new TransformCollection(this);
			this._parent = null;
			this._position = pos;
			this._rotation = rotation;
			this._localScale = scale;
			this.position = new Vector3(pos);
			this.rotation = new Quaternion(rotation);
			this.localScale = new Vector3(scale);
			if (type == Space.world)
				this._updateFromLocal();
			else
			{
				this._localPosition = pos;
				this._localRotation = rotation;
				this._localScale = scale;
				this._updateFromLocal();
			}
		}
		public Transform()
		{
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this._parent = null;
			this.children = new TransformCollection(this);
			this.position = Vector3.zero;
			this.rotation = Quaternion.identity;
			this.localScale = Vector3.one;
			this._updateFromLocal();
		}

		public Transform LookAt(Transform target, Vector3 up)
		{
			Vector3 position = target.transform.position;
			Matrix matrix = Matrix.Identity;
			Vector3 forward = position - this.transform.position;
			Vector3 newup = up;
			if (parent != null)
			{
				forward = parent.worldToLocalMatrix * forward;
				newup = parent.worldToLocalMatrix * up;
			}
			float length = (float)Mathf.Sqrt( (forward[0] * forward[0]) + (forward[1] * forward[1]) + (forward[2] * forward[2]) );
			if ( length != 0 ) {
				forward[0] /= length;
				forward[1] /= length;
				forward[2] /= length;
			} else {
				forward[0] = Vector3.forward.x;
				forward[1] = Vector3.forward.y;
				forward[2] = Vector3.forward.z;
			}
			length = (float)Mathf.Sqrt( up[0] * up[0] + up[1] * up[1] + up[2] * up[2] );
			if ( length != 0) {
				newup[0] /= length;
				newup[1] /= length;
				newup[2] /= length;
			} else {
				newup[0] = Vector3.up.x;
				newup[1] = Vector3.up.y;
				newup[2] = Vector3.up.z;
			}
			Vector3 left = Vector3.Cross(newup, forward);
			if ( left[0]==0 && left[1]==0 && left[2]==0 ) {
				left[0] = transformation[0];
				left[1] = transformation[1];
				left[2] = transformation[2];
			}
			newup = Vector3.Cross( forward, left );
			matrix[0] = left[0];
			matrix[1] = left[1];
			matrix[2] = left[2];
			matrix[3] = 0;
			matrix[4] = newup[0];
			matrix[5] = newup[1];
			matrix[6] = newup[2];
			matrix[7] = 0;
			matrix[8] = forward[0];
			matrix[9] = forward[1];
			matrix[10] = forward[2];
			matrix[11] = 0;
			matrix[12] = 0;
			matrix[13] = 0;
			matrix[14] = 0;
			matrix[15] = 1;
			
			Matrix translation = Matrix.translation(this._localPosition);
			this._localRotation = matrix.quaternion;
			Matrix rotation = matrix;
			Matrix scale = Matrix.scale(this.localScale);
			this._transformation = translation * rotation * scale;
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return (this);
		}

		public Quaternion transformRotation(Quaternion direction)
		{
			return (this.transform.rotation * direction);
		}
		public Quaternion inverseTransformRotation(Quaternion direction)
		{
			return (this.transform.rotation.inverse * direction);
		}

		public Transform LookAt(Vector3 worldPosition, Vector3 up)
		{
			Vector3 position = worldPosition;
			Matrix matrix = Matrix.Identity;
			Vector3 forward = position - this.transform.position;
			Vector3 newup = up;
			if (parent != null)
			{
				forward = parent.worldToLocalMatrix * forward;
				newup = parent.worldToLocalMatrix * up;
			}
			float length = (float)Mathf.Sqrt( (forward[0] * forward[0]) + (forward[1] * forward[1]) + (forward[2] * forward[2]) );
			if ( length != 0 ) {
				forward[0] /= length;
				forward[1] /= length;
				forward[2] /= length;
			} else {
				forward[0] = Vector3.forward.x;
				forward[1] = Vector3.forward.y;
				forward[2] = Vector3.forward.z;
			}
			length = (float)Mathf.Sqrt( up[0] * up[0] + up[1] * up[1] + up[2] * up[2] );
			if ( length != 0 ) {
				newup[0] /= length;
				newup[1] /= length;
				newup[2] /= length;
			} else {
				newup[0] = Vector3.up.x;
				newup[1] = Vector3.up.y;
				newup[2] = Vector3.up.z;
			}
			Vector3 left = Vector3.Cross(newup, forward);
			if ( left[0]==0 && left[1]==0 && left[2]==0 ) {
				left[0] = transformation[0];
				left[1] = transformation[1];
				left[2] = transformation[2];
			}
			newup = Vector3.Cross( forward, left );
			matrix[0] = left[0];
			matrix[1] = left[1];
			matrix[2] = left[2];
			matrix[3] = 0;
			matrix[4] = newup[0];
			matrix[5] = newup[1];
			matrix[6] = newup[2];
			matrix[7] = 0;
			matrix[8] = forward[0];
			matrix[9] = forward[1];
			matrix[10] = forward[2];
			matrix[11] = 0;
			matrix[12] = 0;
			matrix[13] = 0;
			matrix[14] = 0;
			matrix[15] = 1;
			Matrix translation = Matrix.translation(this._localPosition);
			this._localRotation = matrix.quaternion;
			Matrix rotation = matrix;
			Matrix scale = Matrix.scale(this.localScale);
			this._transformation = translation * rotation * scale;
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return (this);
		}
		public void LookAt(Vector3 position, Vector3 up, Space type)
		{
			Matrix matrix = Matrix.Identity;
			if (this.transform == null)
			{
				MessageBox.Show("nulll");
			}
			Vector3 forward = position - this.transform.position;//
			Vector3 newup = up;
			if (parent != null && type == Space.world)
			{
				forward = parent.worldToLocalMatrix * forward;
				newup = parent.worldToLocalMatrix * up;
			}
			else if (type == Space.self)
			{
				forward = position - this.transform.localPosition;
				newup = up;
			}
			float length = (float)Mathf.Sqrt( (forward[0] * forward[0]) + (forward[1] * forward[1]) + (forward[2] * forward[2]) );
			if ( length != 0 ) {
				forward[0] /= length;
				forward[1] /= length;
				forward[2] /= length;
			} else {
				forward[0] = Vector3.forward.x;
				forward[1] = Vector3.forward.y;
				forward[2] = Vector3.forward.z;
			}
			length = (float)Mathf.Sqrt( up[0] * up[0] + up[1] * up[1] + up[2] * up[2] );
			if ( length != 0 ) {
				newup[0] /= length;
				newup[1] /= length;
				newup[2] /= length;
			} else {
				newup[0] = Vector3.up.x;
				newup[1] = Vector3.up.y;
				newup[2] = Vector3.up.z;
			}
			Vector3 left = Vector3.Cross(newup, forward);
			if ( left[0]==0 && left[1]==0 && left[2]==0 ) {
				left[0] = transformation[0];
				left[1] = transformation[1];
				left[2] = transformation[2];
			}
			newup = Vector3.Cross( forward, left );
			matrix[0] = left[0];
			matrix[1] = left[1];
			matrix[2] = left[2];
			matrix[3] = 0;
			matrix[4] = newup[0];
			matrix[5] = newup[1];
			matrix[6] = newup[2];
			matrix[7] = 0;
			matrix[8] = forward[0];
			matrix[9] = forward[1];
			matrix[10] = forward[2];
			matrix[11] = 0;
			matrix[12] = 0;
			matrix[13] = 0;
			matrix[14] = 0;
			matrix[15] = 1;
			Matrix translation = Matrix.translation(this._localPosition);
			this._localRotation = matrix.quaternion;
			Matrix rotation = matrix;
			Matrix scale = Matrix.scale(this.localScale);
			this._transformation = translation * rotation * scale;
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return;
		}
		public void goFrontTo(Transform target)
		{
			Vector3 planeNormal = transform.forward;
			Vector3 planeOrigin = target.position;
			if (this.parent != null)
			{
				planeOrigin = parent.inverseTransformPoint(target.position);
				planeNormal = (this.transformation * (-1 * Vector3.forward)).normalized;
			}
			Vector3 rayOrigin = this._localPosition;
			Vector3 rayDirection = planeNormal;
			Vector3 intersection = Mathf.intersectPlane(planeNormal, planeOrigin, rayOrigin, rayDirection);
			Vector3 translation = planeOrigin - intersection;
			this._localPosition += translation;
			this._updateFromLocal();
		}
		public Vector3 transformPoint(Vector3 localPosition)
		{
			Vector3 res = this.localToWorldMatrix * localPosition;
			return (res);
		}
		public Vector3 inverseTransformPoint(Vector3 worldPosition)
		{
			Vector3 res = this.worldToLocalMatrix * worldPosition;
			return (res);
		}
		public Transform(Transform other)
		{
			Transforms.Add(this);
			this.position = other.position;
			this.rotation = other.rotation;
			this.localScale = Vector3.one;
			this._updateFromLocal();
		}
		public static Transform zero
		{
			get
			{
				return (new Transform(Vector3.zero, Quaternion.identity, Vector3.one));
			}
		}

		/*
			

			Transform();
			Transform(const Transform& copy);
			void			DetachChildren();
			Transform		Find(std::string childName);
			Transform		GetChild(std::size_t index);
			std::size_t		GetSiblingIndex(); //son propre index dans son parent
			Quaternion		*InverseTransformDirection(Quaternion direction);
			Vector3			*InverseTransformPoint(Vector3 position);
			Vector3			*InverseTransformVector(Vector3 vector); // world to local;
			
			void			LookAt(Vector3 worldPosition);
			void			LookAt(Vector3 position, Space space);
			void			Rotate(Vector3 eulerAngles, Space space);
			void			Rotate(Vector3 eulerAngles);
			void			RotateAround(Vector3 point, Vector3 axis, float angle);
			void			SetAsFirstSibling();
			void			SetAsLastSibling();
			void			SetParent(Transform parent);
			void			SetSiblingIndex(int index);
			Quaternion		*TransformDirection(Quaternion *direction); //local to world;
			Vector3			*TransformPosition(Vector3 *direction); //local to world;		
			Quaternion		*TransformVector(Vector3 *vector);
			void			Translate(Vector3 *translation);
		private:
			bool			_updated;
			void			checkUpdate()
			{
				if (this._updated)
				{
					this._updated = false;
					Matrix		*translation = Matrix::Translation(this.localPosition);
					Matrix		*rotation = Matrix::Rotation(this.localRotation);
					Matrix		*scale = Matrix::Scale(this.localScale);
					this.transformation = (*scale) * (*rotation) * (*translation);
				}
			}
			Matrix			*transformation;
	};*/
	}
}
