using System;
using Assimp;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OctogonEngine;
using Vector3 = OctogonEngine.Vector3;
using Quaternion = OctogonEngine.Quaternion;
using Matrix = OctogonEngine.Matrix;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using Assimp.Configs;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OctogonEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using OpenTK.Platform;
using Config = OpenTK.Configuration;
using Utilities = OpenTK.Platform.Utilities;
using Assimp;
using System.Diagnostics;

using System.Drawing;
using Mesh = OctogonEngine.Mesh;
using Material = Assimp.Material;



/*
 * GetAttributes from shaderID; (here "(this as IGraphicResource).Id")
	IEnumerable<VertexAttribute> DetectAttributes()
        {
            int max_attribute_length;
            GL.GetProgram((this as IGraphicsResource).Id, GetProgramParameterName.ActiveAttributeMaxLength, out max_attribute_length);
            StringBuilder name = new StringBuilder(2 * max_attribute_length); // Multiply by 2 because of truncated strings on Mono 1.9.1.
 
            int num_attributes;
            GL.GetProgram((this as IGraphicsResource).Id, GetProgramParameterName.ActiveAttributes, out num_attributes);
 
            for (int index = 0; index < num_attributes; index++)
            {
                int length, size;
                ActiveAttribType type;
                name.Remove(0, name.Length);
                GL.GetActiveAttrib((this as IGraphicsResource).Id, index, name.Capacity, out length, out size, out type, name);
 
                if (length > 0)
                {
                    int location = GL.GetAttribLocation((this as IGraphicsResource).Id, name.ToString());
                    var attr = new VertexAttribute(name.ToString(), location, type);
                    Debug.Print("Attribute detected {0} {1} -> {2}", attr.AttributeType, attr.Name, attr.Index);                  
                    yield return attr;
                }
            }
        }
 */

namespace OctogonEngine
{
	public struct Byte4
	{
		public byte R, G, B, A;

		public Byte4(byte[] input)
		{
			R = input[0];
			G = input[1];
			B = input[2];
			A = input[3];
		}

		public uint ToUInt32()
		{
			byte[] temp = new byte[] { this.R, this.G, this.B, this.A };
			return BitConverter.ToUInt32(temp, 0);
		}

		public override string ToString()
		{
			return this.R + ", " + this.G + ", " + this.B + ", " + this.A;
		}
	}
	

	public class Scene : GameWindow
	{
		public Transform arrowsTransform = Transform.zero;
		public static Scene current;
		public bool check = false;
		public Transform appendAsset(Assimp.Scene model)
		{
			Assimp.Node current = model.RootNode;
			return (appendNodeRecursive(current, null, model));
		}
		public List<Transform> roots = new List<Transform>();

		public static void Main(string[] args)
		{
			using (Scene gameScene = new Scene())
			{
				gameScene.Run();
			}
		}

		public Transform appendNodeRecursive(Assimp.Node node, Transform current, Assimp.Scene m_model)
		{
			Transform tr = getTransform(node, current, m_model);
			Transform children = null;
			if (current == null)
				children = tr;
			else
				children = current.children[current.children.Count - 1];
			if (node.Children != null)
			{
				for (int i = 0; i < node.Children.Count; i++)
				{
					appendNodeRecursive(node.Children[i], children, m_model);
				}
			}
			return (tr);
		}

		protected override void OnResize(EventArgs e)
		{
			// Set orthographic rendering (useful when you want 2D)
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(this.ClientRectangle.Left, this.ClientRectangle.Right,
				this.ClientRectangle.Bottom, this.ClientRectangle.Top, -1.0, 1.0);
			GL.Viewport(this.ClientRectangle.Size);
		}

		public Transform getTransform(Assimp.Node node, Transform parent, Assimp.Scene m_model)
		{
			Transform tr = new Transform(OctogonEngine.Vector3.zero, OctogonEngine.Quaternion.identity, OctogonEngine.Vector3.one);
			if (parent == null && !roots.Contains(tr))
			{
				roots.Add(tr);
			}
			tr.m_model = m_model;
			tr.parent = parent;
			tr.name = node.Name;
			Vector3D scaling;
			Vector3D translation;
			Assimp.Quaternion rotation;

			node.Transform.Decompose(out scaling, out rotation, out translation);
			tr.localRotation = new OctogonEngine.Quaternion(rotation);
			tr.localPosition = new OctogonEngine.Vector3(translation);
			tr.localScale = new OctogonEngine.Vector3(scaling);
			for (int i = 0; i < node.MeshIndices.Count; i++)
			{
				OctogonEngine.Mesh tmp = tr.addComponent<OctogonEngine.Mesh>();
				tmp.Assimp_Import(m_model.Meshes[node.MeshIndices[i]], m_model);
			}
			return (tr);
		}
		public List<Assimp.Scene> m_models = new List<Assimp.Scene>();
		private OpenTK.Vector3 m_sceneCenter, m_sceneMin, m_sceneMax;
		public float m_angle;
		private int m_displayList;
		private int m_texId;
		public Transform worldBegining = new Transform(OctogonEngine.Vector3.zero, OctogonEngine.Quaternion.identity, OctogonEngine.Vector3.one);
		public Transform pivotPoint = Transform.zero;
		AssimpContext importer = new AssimpContext();
		public Transform cameraTransform = new Transform(new Vector3(6, 0, 0), Quaternion.identity, Vector3.one);
		public Scene()
		{
			/*String fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "alien2.dae");
			String fileName2 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "handles.dae");
			String fileName3 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "cube.dae");
			String fileName4 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "duck.dae");
			
			Assimp.Scene m_model2;
			Assimp.Scene m_model3;
			importer.SetConfig(new NormalSmoothingAngleConfig(66.0f));
			Assimp.Scene m_model = importer.ImportFile(fileName, PostProcessPreset.TargetRealTimeMaximumQuality);;
			m_model2 = importer.ImportFile(fileName2, PostProcessPreset.TargetRealTimeMaximumQuality);
			appendAsset(m_model);
			arrowsTransform = appendAsset(m_model2);
			Assimp.Scene m_model5 = importer.ImportFile(fileName4, PostProcessPreset.TargetRealTimeMaximumQuality);
			appendAsset(m_model5);
			m_model3 = importer.ImportFile(fileName3, PostProcessPreset.TargetRealTimeMaximumQuality);
			appendAsset(m_model3);
			cameraTransform.LookAt(Vector3.zero, Vector3.up, Space.world);
			current = this;
			ComputeBoundingBox(m_model);
			this.rotationHandles = AppendAsset(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "rotation_handles.dae"));*/

			Transform tr = this.AppendAsset(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "troll.dae"));
			cameraTransform.LookAt(Vector3.zero, Vector3.up, Space.world);
			ComputeBoundingBox(tr.m_model);

		}
		public Transform rotationHandles;
		public Transform AppendAsset(String path)
		{
			if (File.Exists(path))
			{
				Console.WriteLine("FILE EXIST:" + path);
				//if (console != null)
				//	console.Text += path;
				Assimp.Scene m_model = importer.ImportFile(path, PostProcessPreset.TargetRealTimeMaximumQuality);
				return (appendAsset(m_model));
			}
			return (null);
		}

		private void ComputeBoundingBox(Assimp.Scene m_model)
		{
			m_sceneMin = new OpenTK.Vector3(1e10f, 1e10f, 1e10f);
			m_sceneMax = new OpenTK.Vector3(-1e10f, -1e10f, -1e10f);
			Matrix4 identity = Matrix4.Identity;

			ComputeBoundingBox(m_model.RootNode, ref m_sceneMin, ref m_sceneMax, ref identity, m_model);

			m_sceneCenter.X = (m_sceneMin.X + m_sceneMax.X) / 2.0f;
			m_sceneCenter.Y = (m_sceneMin.Y + m_sceneMax.Y) / 2.0f;
			m_sceneCenter.Z = (m_sceneMin.Z + m_sceneMax.Z) / 2.0f;
		}

		private void ComputeBoundingBox(Node node, ref OpenTK.Vector3 min, ref OpenTK.Vector3 max, ref Matrix4 trafo, Assimp.Scene m_model)
		{
			Matrix4 prev = trafo;
			trafo = Matrix4.Mult(prev, FromMatrix(node.Transform));

			if (node.HasMeshes)
			{
				foreach (int index in node.MeshIndices)
				{
					Assimp.Mesh mesh = m_model.Meshes[index];
					for (int i = 0; i < mesh.VertexCount; i++)
					{
						OpenTK.Vector3 tmp = FromVector(mesh.Vertices[i]);
						OpenTK.Vector3.Transform(ref tmp, ref trafo, out tmp);

						min.X = Math.Min(min.X, tmp.X);
						min.Y = Math.Min(min.Y, tmp.Y);
						min.Z = Math.Min(min.Z, tmp.Z);

						max.X = Math.Max(max.X, tmp.X);
						max.Y = Math.Max(max.Y, tmp.Y);
						max.Z = Math.Max(max.Z, tmp.Z);
					}
				}
			}

			for (int i = 0; i < node.ChildCount; i++)
			{
				ComputeBoundingBox(node.Children[i], ref min, ref max, ref trafo, m_model);
			}
			trafo = prev;
		}

		public Vector2 currentScreen
		{
			get
			{
				return (new Vector2(this.Width, this.Height));
			}
			set
			{
				this.Width = (int)value.x;
				this.Height = (int)value.y;
			}
		}
		private int Width;
		private int Height;
		private uint _layerTexture = 0;
		private uint _FboHandle = 0;
		public uint layerTexture
		{
			get
			{
				return (this._layerTexture);
			}
			private set
			{
				this._layerTexture = value;
			}
		}
		public uint FboHandle
		{
			get
			{
				return (this._FboHandle);
			}
			private set
			{
				this._FboHandle = value;
			}
		}
		private void CreateLayer()
		{
			GL.GenTextures(1, out this._layerTexture);
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.Ext.GenFramebuffers(1, out this._FboHandle);
			GL.BindTexture(TextureTarget.Texture2D, 0);
		}
		private void GenerateLayer()
		{
			if (_layerTexture == 0)
			{
				CreateLayer();
			}
			GL.BindTexture(TextureTarget.Texture2D, this._layerTexture);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Clamp);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Clamp);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, Width, Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, FboHandle);
			GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, this._layerTexture, 0);
			GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext);
			GL.ClearColor(Color.White);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
			GL.DrawBuffer(DrawBufferMode.Back);
		}

		private void SaveLayer()
		{
			ActivateLayer();
			int width = Width;
			int height = Height;
			System.Drawing.Imaging.PixelFormat format = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			Bitmap bmp = new Bitmap(width, height, format);
			System.Drawing.Imaging.BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, width, height),
			System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.ReadBuffer(ReadBufferMode.ColorAttachment0);
			GL.ReadPixels(0, 0, width, height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
			bmp.UnlockBits(data);
			bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
			bmp.Save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "screenshot.bmp"));
		}
		//public GLControl currentControl;
		private void ActivateLayer()
		{
			GL.BindTexture(TextureTarget.Texture2D, this._layerTexture);
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, FboHandle);
			GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, this._layerTexture, 0);
			GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext);
		}
		private void DesactivateLayer()
		{
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
			GL.DrawBuffer(DrawBufferMode.Back);
		}
		//public Form1 form;

		List<int> displays = new List<int>();
		public Quaternion testrotation = Quaternion.identity;
		public void OnRenderFrame()
		{
			baseColor = FromColor(Color.LightGray);
			GL.ClearColor(Color.SlateGray);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);


			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();
			Matrix4 lookat2;
			lookat2 = cameraTransform.worldToLocalMatrix.gl();
			lookat2.Transpose();
			GL.LoadMatrix(ref lookat2);
			float tmp = m_sceneMax.X - m_sceneMin.X;
			tmp = Math.Max(m_sceneMax.Y - m_sceneMin.Y, tmp);
			tmp = Math.Max(m_sceneMax.Z - m_sceneMin.Z, tmp);
			tmp = 1.0f / tmp;

			if (displays == null || displays.Count == 0)
			{
				for (int i = 0; i < roots.Count; i++)
				{
					m_displayList = GL.GenLists(1);
					displays.Add(m_displayList);
				}
				for (int i = 0; i < roots.Count; i++)
				{
				GL.LoadIdentity();
				GL.LoadMatrix(ref lookat2);
				GL.NewList(displays[i], ListMode.CompileAndExecute );
				RecursiveRender(roots[i], 0);
				GL.EndList();
				}
			}
			else if (displays.Count < roots.Count)
			{
				while (displays.Count < roots.Count)
				{
					m_displayList = GL.GenLists(1);
					displays.Add(m_displayList);
				}
				for (int i = 0; i < displays.Count; i++)
					GL.DeleteLists(displays[i], 1);
				for (int i = 0; i < roots.Count; i++)
				{
				GL.LoadIdentity();
				GL.LoadMatrix(ref lookat2);
				GL.NewList(displays[i], ListMode.CompileAndExecute);
				RecursiveRender(roots[i], 0);
				GL.EndList();
				}
			}
			for (int i = 0; i < roots.Count; i++)
			{
				GL.LoadIdentity();
				GL.LoadMatrix(ref lookat2);
				RecursiveRender(roots[i], 0);
			}
		}
		private Color4 baseColor;

		//public System.Windows.Forms.RichTextBox console;
		Matrix last = Matrix.Identity;
		private void RecursiveRender(Transform node, int mode)
		{

			Matrix4 m = node.transformation.gl();
			
			m.Transpose();
			if (node.isSelected && mode == 0)
			{
				GL.PushName(node.GetHashCode());
				RecursiveRender(node, mode + 1);
				Color4 Transparent = new Color4(0, 55, 0, 55);
				GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Transparent);
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
				GL.LineWidth(0.01f);
				GL.Enable(EnableCap.LineStipple);
				unchecked
				{
					GL.LineStipple(1, (short)(0x3F07F / 2));
				}
			}
			else if (node.isSelected)
			{
				GL.Disable(EnableCap.LineStipple);
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			}
			else
			{
				GL.PushName(node.GetHashCode());
				GL.Disable(EnableCap.LineStipple);
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			}
				
			GL.PushMatrix();
			GL.MultMatrix(ref m);
			Color4 layerColor = FromIntColor((uint)node.GetHashCode());
			if (node.getComponent<OctogonEngine.Mesh>() != null)
			{
				
				Assimp.Scene scene = node.m_model;
				List<OctogonEngine.Mesh> meshs = node.getComponents<OctogonEngine.Mesh>();
				foreach (OctogonEngine.Mesh myMesh in meshs)
				{

					Assimp.Mesh mesh = myMesh.importVersion;
					if (myMesh.materials != null && (!node.isSelected || mode == 1))
					{
						myMesh.materials[0].Bind();
					}

					if (mesh.HasNormals)
					{
						GL.Enable(EnableCap.Lighting);
					}
					else
					{
						GL.Disable(EnableCap.Lighting);
					}

					bool hasColors = mesh.HasVertexColors(0);
					if (hasColors)
					{
						GL.Enable(EnableCap.ColorMaterial);
					}
					else
					{
						GL.Disable(EnableCap.ColorMaterial);
					}


					bool hasTexCoords = mesh.HasTextureCoords(0);

					foreach (Assimp.Face face in mesh.Faces)
					{
						BeginMode faceMode;
						switch (face.IndexCount)
						{
							case 1:
								faceMode = BeginMode.Points;
								break;
							case 2:
								faceMode = BeginMode.Lines;
								break;
							case 3:
								faceMode = BeginMode.Triangles;
								break;
							default:
								faceMode = BeginMode.Polygon;
								break;
						}
						
						
						GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);

						for (int i = 0; i < face.IndexCount; i++)
						{
							int indice = face.Indices[i];
							GL.Color4(baseColor);
							if (hasColors)
							{
								Color4 vertColor = FromIntColor(0);
								if (hasColors)
									vertColor = FromColor(mesh.VertexColorChannels[0][indice]);

								GL.Color4(vertColor);
							}
								if (mesh.HasNormals)
								{
									OpenTK.Vector3 normal = FromVector(mesh.Normals[indice]);
									GL.Normal3(normal);
								}
								if (hasTexCoords)
								{
									OpenTK.Vector3 uvw = FromVector(mesh.TextureCoordinateChannels[0][indice]);
									GL.TexCoord2(uvw.X, 1 - uvw.Y);
								}
							OpenTK.Vector3 pos = FromVector(mesh.Vertices[indice]);
							GL.Vertex3(pos);
						}

						GL.End();
					}

				}

			}
			if (mode == 0)
				GL.PopName();

			if (node.children != null && mode == 0)
			{
				for (int i = 0; i < node.ChildCount; i++)
				{

					RecursiveRender(node.children[i], mode);
				}
			}
			GL.PopMatrix();

		}

		// ! \\ beware do it only once per model and implement shared textures, or memory leaks will occur :'( FORBIDEN TO REMOVE COMMENT!!!!
		/// <summary>
		/// Load Texture in the fuckin stack my lord, only once! not at each frame bastard... 
		/// </summary>
		/// <param name="fileName"></param>
		private void LoadTexture(String fileName)
		{
			fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
			if (!File.Exists(fileName))
			{
				//console.Text += fileName;
				GL.Disable(EnableCap.Texture2D);
				return;
			}
			Bitmap textureBitmap = new Bitmap(fileName);
			BitmapData TextureData =
					textureBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, textureBitmap.Width, textureBitmap.Height),
					System.Drawing.Imaging.ImageLockMode.ReadOnly,
					System.Drawing.Imaging.PixelFormat.Format24bppRgb
				);
			m_texId = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, m_texId);

			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, textureBitmap.Width, textureBitmap.Height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgr, PixelType.UnsignedByte, TextureData.Scan0);
			textureBitmap.UnlockBits(TextureData);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
		}


		private void ApplyMaterial(Assimp.Material mat, Mesh mesh)
		{
			/*
			 
			 *pour l'instant on utilise:
			 * - mat.GetMaterialTexture(TextureType.Diffuse, 0, out tex)
			 * - toutes les couleurs.
			 * - shininess
			 * - strengh
			 
			 */

			if (mat.GetMaterialTextureCount(TextureType.Diffuse) > 0)
			{
				TextureSlot tex;
				if (mat.GetMaterialTexture(TextureType.Diffuse, 0, out tex))
				{
					GL.Enable(EnableCap.Texture2D);
					LoadTexture(tex.FilePath);
				}
				else
				{
					GL.Disable(EnableCap.Texture2D);
				}
			}
			else
				GL.Disable(EnableCap.Texture2D);

			Color4 color = new Color4(.8f, .8f, .8f, 1.0f);
			if (mat.HasColorDiffuse)
			{
				color = FromColor(mat.ColorDiffuse);
			}
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, color);
			color = new Color4(0, 0, 0, 1.0f);
			if (mat.HasColorSpecular)
			{
				color = FromColor(mat.ColorSpecular);
			}
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Specular, color);

			color = new Color4(.2f, .2f, .2f, 1.0f);
			if (mat.HasColorAmbient)
			{
				color = FromColor(mat.ColorAmbient);
			}
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Ambient, color);

			color = new Color4(0, 0, 0, 1.0f);
			if (mat.HasColorEmissive)
			{
				color = FromColor(mat.ColorEmissive);
			}
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Emission, color);

			float shininess = 1;
			float strength = 1;
			if (mat.HasShininess)
			{
				shininess = mat.Shininess;
			}
			if (mat.HasShininessStrength)
			{
				strength = mat.ShininessStrength;
			}

			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Shininess, shininess * strength);
		}

		private Matrix4 FromMatrix(Matrix4x4 mat)
		{
			Matrix4 m = new Matrix4();
			m.M11 = mat.A1;
			m.M12 = mat.A2;
			m.M13 = mat.A3;
			m.M14 = mat.A4;
			m.M21 = mat.B1;
			m.M22 = mat.B2;
			m.M23 = mat.B3;
			m.M24 = mat.B4;
			m.M31 = mat.C1;
			m.M32 = mat.C2;
			m.M33 = mat.C3;
			m.M34 = mat.C4;
			m.M41 = mat.D1;
			m.M42 = mat.D2;
			m.M43 = mat.D3;
			m.M44 = mat.D4;
			return m;
		}

		private OpenTK.Vector3 FromVector(Assimp.Vector3D vec)
		{
			OpenTK.Vector3 v;
			v.X = vec.X;
			v.Y = vec.Y;
			v.Z = vec.Z;
			return v;
		}

		private Color4 FromColor(Color4D color)
		{
			Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}

		private Color4 FromColor(Color color)
		{
			Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}

		private Color4 FromIntColor(uint rgba)
		{
			Color4 C;
			C.R = rgba & 0xFF000000;
			C.G = rgba & 0x00FF0000;
			C.B = rgba & 0x0000FF00;
			C.A = rgba & 0x000000FF;
			return C;
		}
		public uint ToIntColor(Byte4 C)
		{
			uint res = 0;
			res = res | C.A;
			res = res | (uint)(C.B << 8);
			res = res | (uint)(C.G << 16);
			res = res | (uint)(C.R << 24);
			return (res);
		}
	}

}
